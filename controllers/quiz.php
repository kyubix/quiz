<?php


class Quiz extends Controller
{
    protected function index()
    {
        $viewmodel = new QuestionModel();
        $this->returnView($viewmodel->getRandom30Question(), true);
    }

    protected function results(){
        $viewmodel = new QuestionModel();
        $this->returnView($viewmodel->results(),true);
    }
}