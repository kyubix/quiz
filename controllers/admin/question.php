<?php

class Question extends Controller{
    protected function add(){
        $viewmodel = new QuestionModel();
        $this->returnView($viewmodel->add(),true);
    }

    protected function index(){
        $viewmodel = new QuestionModel();
        $this->returnView($viewmodel->index(),true);
    }

    protected function edit(){
        $viewmodel = new QuestionModel();
        $this->returnView($viewmodel->edit($this->id),true);
    }
}