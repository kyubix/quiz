<?php

class Bootstrap
{
    private $controller;
    private $action;
    private $request;
    private $id;

    public function __construct($request)
    {
        $this->request = $request;
        if ($this->request['controller'] == '') {
            $this->controller = 'home';
        } else {
            $this->controller = $this->request['controller'];
        }

        if ($this->request['action'] == '') {
            $this->action = 'index';
        } else {
            $this->action = $this->request['action'];
        }
        if ($this->request['id'] != '') {
            $this->id = $this->request['id'];
        }
        //echo $this->request['id'];
    }

    public function createController()
    {
        //check class
        //echo $this->controller;
        //echo class_exists($this->controller);
        if (class_exists($this->controller)) {
            $parents = class_parents($this->controller);
            //check extend
            if (in_array("Controller", $parents)) {
                if (method_exists($this->controller, $this->action)) {
                    $controller = new $this->controller($this->action, $this->request);
                    if ($this->id != '')
                        $controller->setId($this->id);
                    return $controller;
                } else {
                    echo '<h1>method doesnt exist</h1>';
                    return;
                }
            } else {
                //base controller does not exist
                echo '<h1>Base controller not found</h1>';
                return;
            }
        } else {
            //controller class does not exist
            echo '<h1>controller class doesn\'t exist</h1>';
            return;
        }
    }
}