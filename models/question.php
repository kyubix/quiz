<?php

class QuestionModel extends Model{
    public function add(){
        $this->checkIfAdmin();

        if($_POST['submit']){

            if($this->checkIfStringsEmpty())
                return;

            if($this->checkIfIsAnyCorrect())
                return;



            if($_FILES['questionImage']['name']!=''){

                $fhandle = fopen($_FILES['questionImage']['tmp_name'],'r');
                $content = fread($fhandle,$_FILES['questionImage']['size']);
                fclose($fhandle);

                $this->query('INSERT INTO pytanie(tresc, id_uzytkownik, zdjecie) VALUES(:tresc,:id_uzytkownik,:zdjecie)');
                $this->bind(':tresc',$_POST['question']);
                $this->bind(':id_uzytkownik',$_SESSION['user_data']['id']);
                $this->bind(':zdjecie',$content);
                $this->execute();
                $questionId = $this->lastInsertId();
                if($questionId){
                    $this->addAnswer($questionId,1);
                    $this->addAnswer($questionId,2);
                    $this->addAnswer($questionId,3);
                    $this->addAnswer($questionId,4);
                }else{
                    Messages::setMsg('Coś poszło nie tak, spróbuj ponownie','error');
                    return;
                }

            }else {
                $this->query('INSERT INTO pytanie(tresc, id_uzytkownik) VALUES(:tresc,:id_uzytkownik)');
                $this->bind(':tresc',$_POST['question']);
                $this->bind(':id_uzytkownik',$_SESSION['user_data']['id']);
                $this->execute();
                $questionId = $this->lastInsertId();
                if($questionId){
                    $this->addAnswer($questionId,1);
                    $this->addAnswer($questionId,2);
                    $this->addAnswer($questionId,3);
                    $this->addAnswer($questionId,4);
                }else{
                    Messages::setMsg('Coś poszło nie tak, spróbuj ponownie','error');
                    return;
                }
            }
            header('Location: ' . ROOT_URL. '/admin/question/index');

        }
        return;
    }

    private function addAnswer($questionId, $number){
        $this->query('INSERT INTO odpowiedz(tresc, czy_poprawne, id_pytanie) VALUES(:tresc, :czy_poprawne, :id_pytanie)');
        $this->bind(':tresc',$_POST['answer'.$number]);
        $this->bind(':czy_poprawne',$_POST['isCorrect'.$number]=='on'?true:false);
        $this->bind(':id_pytanie',$questionId);
        $this->execute();

    }

    public function index(){
        $this->checkIfAdmin();

        if($_POST['submit']){
            $this->deleteQuestion($_POST['deleteItemId']);
            header('Location: ' . ROOT_URL . '/admin/question/index');
        }


        $this->query('SELECT * FROM pytanie');
        $rows = $this->resultSet();
        $this->query('SELECT * FROM odpowiedz');
        $answers = $this->resultSet();
        $i=1;
        $j=0;
        foreach($rows as $row){
            foreach($answers as $answer){

                if($row['id']==$answer['id_pytanie']){
                    $rows[$j]['answer'.$i]=$answer;
                    $i++;
                }
            }
            $i=1;
            $j++;
        }
        //print_r($rows);
        return $rows;
    }

    private function deleteQuestion($id){
        $this->query('DELETE FROM odpowiedz WHERE id_pytanie=:id');
        $this->bind(':id',$id);
        $this->execute();
        $this->query('DELETE FROM pytanie WHERE id=:id');
        $this->bind(':id',$id);
        $this->execute();
    }

    public function edit($id){
        $this->checkIfAdmin();

        if($_POST['submit']){
            if($this->checkIfStringsEmpty())
                return;

            if($this->checkIfIsAnyCorrect())
                return;
            //print_r($_POST);
            //print_r($_FILES);
            if($_FILES['questionImage']['name']!='') {
                $fhandle = fopen($_FILES['questionImage']['tmp_name'],'r');
                $content = fread($fhandle,$_FILES['questionImage']['size']);
                fclose($fhandle);

                $this->query('UPDATE pytanie SET tresc=:tresc, zdjecie=:zdjecie WHERE id=:id');
                $this->bind(':tresc', $_POST['question']);
                $this->bind(":zdjecie",$content);
                $this->bind(':id', $id);
                $this->execute();
                $this->updateAnswer(1);
                $this->updateAnswer(2);
                $this->updateAnswer(3);
                $this->updateAnswer(4);
                header('Location: ' . ROOT_URL . '/admin/question/index');
            }else{
                $this->query('UPDATE pytanie SET tresc=:tresc WHERE id=:id');
                $this->bind(':tresc', $_POST['question']);
                $this->bind(':id', $id);
                $this->execute();
                $this->updateAnswer(1);
                $this->updateAnswer(2);
                $this->updateAnswer(3);
                $this->updateAnswer(4);
                header('Location: ' . ROOT_URL . '/admin/question/index');
            }

            if($_POST['deleteImage']=='on'){
                $this->query('UPDATE pytanie SET zdjecie=:zdjecie WHERE id=:id');
                $this->bind(':zdjecie',null);
                $this->bind(':id',$id);
                $this->execute();
            }

        }
        $this->query('SELECT * FROM pytanie WHERE id=:id');
        $this->bind(':id',$id);
        $row = $this->single();
        $this->query('SELECT * from odpowiedz WHERE id_pytanie=:id');
        $this->bind(':id',$id);
        $answers = $this->resultSet();
        $row['answers'] = $answers;
        //print_r($row);
        return $row;
    }

    private function updateAnswer($number){
        $this->query('UPDATE odpowiedz SET tresc=:tresc, czy_poprawne=:czy_poprawne WHERE id=:id');
        $this->bind(':tresc',$_POST['answer'.$number]);
        $this->bind(':czy_poprawne',$_POST['isCorrect'.$number]=='on'?true:false);
        $this->bind(':id',$_POST['answerId'.$number]);
        $this->execute();

    }

    private function checkIfAdmin(){
        if($_SESSION['user_data']['role']!='ADMIN'){
            header('Location: '.ROOT_URL);
            return;
        }
    }

    private function checkIfStringsEmpty(){
        if($_POST['question']=='' ||$_POST['answer1']=='' ||$_POST['answer2']=='' ||
            $_POST['answer3']=='' ||$_POST['answer4']==''){

            Messages::setMsg('Treść pytania, oraz odpowiedzi nie moga być puste','error');
            return true;
        }
        return false;
    }

    private function checkIfIsAnyCorrect(){
        if($_POST['isCorrect1']!='on' &&$_POST['isCorrect2']!='on' &&
            $_POST['isCorrect3']!='on' &&$_POST['isCorrect4']!='on'){
            Messages::setMsg('Przynajmniej jedna odpowiedź musi być prawdziwa','error');
            return true;
        }
        return false;
    }

    public function getRandom30Question(){

        if($_SESSION['is_logged_in']==false)
            header('Location: ' . ROOT_URL);



        if($_POST['submit']){

            if($_SESSION['i']<29){
                $_SESSION['i']=$_SESSION['i']+1;

                $_SESSION['answers'] = array_merge($_SESSION['answers'],$_POST);
                //print_r($_SESSION['answers']);

            }else {
                $_SESSION['answers'] = array_merge($_SESSION['answers'],$_POST);

                $markedAnswersId = $this->getIdsFromKeys($this->getOnlyIsCorrectValues($_SESSION['answers']));
                $query = 'SELECT DISTINCT id_pytanie FROM odpowiedz WHERE id IN (';

                foreach ($markedAnswersId as $id) {
                    $query = $query . $id . ',';
                }

                $query = substr_replace($query, ')', strrpos($query, ','), 1);
                $this->query($query);
                $questionIds = $this->resultSet();


                $correctQuestionIds = $this->checkIfUserMarkedCorrect($questionIds, $markedAnswersId);
                $correctQuestionIds = $this->checkIfUserMarkedIncorrectAnswer($correctQuestionIds,$markedAnswersId);

                $_SESSION['$markedByUserIds'] = $markedAnswersId;
                $_SESSION['questionIds'] = $questionIds;
                $_SESSION['correctQuestionIds'] = $correctQuestionIds;

                $this->getMark();

                header('Location: ' . ROOT_URL . '/quiz/results');
            }
        }
        if($_SESSION['i']==0) {
            $this->query('SELECT * FROM pytanie ORDER BY rand() LIMIT 30');
            $rows = $this->resultSet();

            $query = 'SELECT * FROM odpowiedz WHERE id_pytanie IN (';
            foreach ($rows as $row) {
                $query = $query . $row['id'] . ',';
            }

            $query = substr_replace($query, ')', strrpos($query, ','), 1);
            $this->query($query);
            $answers = $this->resultSet();

            $i = 1;
            $j = 0;
            foreach ($rows as $row) {
                foreach ($answers as $answer) {

                    if ($row['id'] == $answer['id_pytanie']) {
                        $rows[$j]['answer' . $i] = $answer;
                        $i++;
                    }
                }
                $i = 1;
                $j++;
            }
            $_SESSION['rows']=$rows;
        }
        //print_r($rows);
        return $_SESSION['rows'];
    }

    public function results(){
        if($_SESSION['is_logged_in']==false || $_SESSION['i']!=29)
            header('Location: ' . ROOT_URL);

        $_SESSION['i']=0;

        $query='SELECT * FROM pytanie WHERE id IN (';
        $questionIds = $_SESSION['questionIds'];
        $markedByUserIds = $_SESSION['$markedByUserIds'];
        $correctQuestionIds=$_SESSION['correctQuestionIds'];
        foreach($questionIds as $id){
            $query=$query.$id['id_pytanie'].',';
        }

        $query = substr_replace($query,')',strrpos($query,','),1);
        $this->query($query);
        $questions = $this->resultSet();

        $query = 'SELECT * FROM odpowiedz WHERE id_pytanie IN (';
        foreach($questions as $row){
            $query=$query.$row['id'].',';
        }

        $query = substr_replace($query,')',strrpos($query,','),1);
        $this->query($query);
        $answers = $this->resultSet();

        $i=1;
        $j=0;
        foreach($questions as $row){
            in_array($row['id'],$correctQuestionIds)?$questions[$j]['isGood']=true:$questions[$j]['isGood']=false;

            foreach($answers as $answer){

                if($row['id']==$answer['id_pytanie']){
                    in_array($answer['id'],$markedByUserIds)?$answer['marked']=true:$answer['marked']=false;
                    $questions[$j]['answer'.$i]=$answer;
                    $i++;
                }
            }
            $i=1;
            $j++;
        }

        //print_r($questions);
        return $questions;
    }

    private function getOnlyIsCorrectValues($arg){
        $mSearch='isCorrect';
        $allowed=array_filter(
            array_keys($arg),
            function($key) use ($mSearch){
                return stristr($key,$mSearch);
            });
        return array_intersect_key($arg,array_flip($allowed));
    }

    private function getIdsFromKeys($arg){
        $result_arr = array();
        foreach($arg as $key=>$value){
            array_push($result_arr,substr($key,9));
        }
        return $result_arr;
    }

    private function checkIfUserMarkedCorrect($questionIds, $markedAnswerIds){
        $correctQuestionIds = array();
        foreach($questionIds as $questionId){
            $this->query('SELECT id FROM odpowiedz WHERE id_pytanie=:id_pytanie AND czy_poprawne=true');
            $this->bind(':id_pytanie',$questionId['id_pytanie']);
            $correctAnswers = $this->resultSet();
            $amountOfCorrect = count($correctAnswers);
            $correctNumber=0;
            foreach($correctAnswers as $correctAnswer){
                foreach($markedAnswerIds as $markedAnswerId){
                    if($correctAnswer['id']==$markedAnswerId){
                        $correctNumber++;
                    }
                }
            }

            if($correctNumber==$amountOfCorrect){
                array_push($correctQuestionIds,$questionId['id_pytanie']);
            }
        }
        return $correctQuestionIds;
    }

    private function checkIfUserMarkedIncorrectAnswer($correctQuestions,$markedAnswers){

        $query = 'SELECT * FROM odpowiedz WHERE czy_poprawne=0 AND id IN (';

        foreach ($markedAnswers as $id) {
            $query = $query . $id . ',';
        }

        $query = substr_replace($query, ')', strrpos($query, ','), 1);
        $this->query($query);
        $wrongAnswers = $this->resultSet();

        foreach($wrongAnswers as $wrong){
            if(in_array($wrong['id_pytanie'],$correctQuestions)){
                $correctQuestions = array_diff($correctQuestions,array($wrong['id_pytanie']));
            }
        }
        return $correctQuestions;

    }

    private function getMark(){
        $amountOfPoints = count($_SESSION['correctQuestionIds']);
        if($amountOfPoints<=17){
            $_SESSION['mark'] = '2.0';
        } elseif($amountOfPoints>17 && $amountOfPoints<=19){
            $_SESSION['mark'] = '3.0';
        } elseif($amountOfPoints>19 && $amountOfPoints<=22){
            $_SESSION['mark'] = '3.5';
        } elseif ($amountOfPoints>22 && $amountOfPoints<=25){
            $_SESSION['mark'] = '4.0';
        } elseif($amountOfPoints>25 && $amountOfPoints<=28){
            $_SESSION['mark'] = '4.5';
        } else {
            $_SESSION['mark'] = '5.0';
        }
    }


}