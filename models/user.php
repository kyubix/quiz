<?php

class UserModel extends Model{
    public function register(){
       // $this->query("select * from rola");
        //$this->execute();
       // echo $this->rowCount();
        //sanitize post
        $post = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);



        $password = md5($post['password']);
        if($post['submit']){

            if($post['login'] == '' || $post['email'] == '' || $post['password'] == ''){
                Messages::setMsg('Uzupełnij wszystkie pola','error');
                return;
            }

            $this->query("select * from uzytkownik where login=:login");
            $this->bind(':login',$post['login']);
            $this->execute();
            if($this->rowCount()>0){
                Messages::setMsg('Login zajęty','error');
                return;
            }

            $this->query("select * from uzytkownik where email=:email");
            $this->bind(':email',$post['email']);
            $this->execute();
            if($this->rowCount()>0){
                Messages::setMsg('Email zajęty','error');
                return;
            }


            //insert into db
            $this->query('INSERT INTO uzytkownik (login, email, haslo, id_rola) values (:login,:email,:haslo, :id_rola)');
            $this->bind(':login',$post['login']);
            $this->bind(':email',$post['email']);
            $this->bind(':haslo',$password);
            $this->bind(':id_rola',2);
            $this->execute();


        }
        return;
    }

    public function login(){
        //sanitize post
        $post = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

        if($post['submit']){

            if($post['login'] == '' || $post['password'] == ''){
                Messages::setMsg('Uzupełnij wszystkie pola','error');
                return;
            }
            $password = md5($post['password']);
            //compare
            $this->query('SELECT * FROM uzytkownik WHERE login = :login AND haslo = :haslo');
            $this->bind(':login',$post['login']);
            $this->bind(':haslo',$password);

            $row = $this->single();

            if($row){
                $this->query('SELECT * FROM rola WHERE id=:id');
                $this->bind(':id',$row['id_rola']);
                $role = $this->single();
                if($role) {
                    $_SESSION['is_logged_in'] = true;
                    $_SESSION['user_data'] = array(
                        "id" => $row['id'],
                        "login" => $row['login'],
                        "email" => $row['email'],
                        "role" => $role['nazwa']
                    );

                    header('Location: ' . ROOT_URL);

                }
                else{
                    Messages::setMsg('Coś poszło nie tak, spróbuj ponownie lub skontaktuj się z administratorem',
                        'error');
                }
            } else {
                Messages::setMsg('Błędny login lub hasło','error');
            }
        }
        return;
    }
}