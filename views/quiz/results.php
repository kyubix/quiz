<div class="quiz-header container">
    <h4>Wyniki egzaminu</h4>
    <p>Poniżej znajdują się Twoje odpowiedzi do pytań z informacją, czy otrzymałeś punkt za dane pytanie.</p>
    <hr />
    <br />
</div>
<div class="quiz col-md-10 offset-md-1">
    <!-- Modal -->
    <div class="modal fade" id="quizResult" tabindex="-1" role="dialog" aria-labelledby="resultTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="resultTitle">Wynik testu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>Poprawne odpowiedzi: <strong><?= count($_SESSION['correctQuestionIds']); ?>/30</strong></span>
                    <span>Twoja ocena to: <strong><?= $_SESSION['mark']; ?><strong></span>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" href="<?php echo ROOT_URL; ?>"><i class="fa fa-home"></i> Start</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-check"></i> Pokaż odpowiedzi</button>
                </div>
            </div>
        </div>
    </div>
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">

            <?php foreach($viewmodel as $item) : ?>
            <div class="card">
                <?php if($item['isGood']) :?>
                    <div class="card-header bg-success text-light">Poprawna odpowiedź</div>
                <?php else : ?>
                    <div class="card-header bg-danger text-light">Błędna odpowiedź</div>
                <?php endif; ?>
                <div class="card-body">
                    <h5><?php echo htmlentities($item['tresc']);?></h5>
                    <hr/>
                        <?php if($item['zdjecie']!= '') : ?>
                            <div class="col-lg-6 d-inline-block form-group question-form float-right">
                            <img class="img-thumbnail" src="data:image/jpeg;base64,<?php echo base64_encode($item['zdjecie']);?>" alt="">
                        </div>
                        <?php endif; ?>
                    <?php if($item['zdjecie']!= '') : ?>
                    <div class="col-lg-5 d-inline-block float-left">
                        <?php else : ?>
                        <div class="col-lg-12 d-inline-block">
                            <?php endif; ?>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect1<?php echo $item['id']; ?>" id="isCorrect1<?php echo $item['id']; ?>" disabled
                                <?= $item['answer1']['marked']?'checked':'';?>/>
                            <label for="isCorrect1<?php echo $item['id']; ?>"></label>
                            <input type="hidden" name="answerId1" value="<?php echo $item['answers'][0]['id'];?>" />
                            <div class="col-10 px-0">
                                <span>
                                    <?php if($item['answer1']['czy_poprawne']) : ?>
                                    <strong><?php echo htmlentities($item['answer1']['tresc']);?></strong>
                                    <?php else : ?>
                                    <?php echo htmlentities($item['answer1']['tresc']);?>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </div>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect2<?php echo $item['id']; ?>" id="isCorrect2<?php echo $item['id']; ?>" disabled
                                <?= $item['answer2']['marked']?'checked':'';?>/>
                            <label for="isCorrect2<?php echo $item['id']; ?>"></label>
                            <input type="hidden" name="answerId1" value="<?php echo $item['answers'][1]['id'];?>" />
                            <div class="col-10 px-0">
                                <span>
                                    <?php if($item['answer2']['czy_poprawne']) : ?>
                                        <strong><?php echo htmlentities($item['answer2']['tresc']);?></strong>
                                    <?php else : ?>
                                        <?php echo htmlentities($item['answer2']['tresc']);?>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </div>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect3<?php echo $item['id']; ?>" id="isCorrect3<?php echo $item['id']; ?>" disabled
                                <?= $item['answer3']['marked']?'checked':'';?>/>
                            <label for="isCorrect3<?php echo $item['id']; ?>"></label>
                            <input type="hidden" name="answerId1" value="<?php echo $item['answers'][2]['id'];?>" />
                            <div class="col-10 px-0">
                                <span>
                                     <?php if($item['answer3']['czy_poprawne']) : ?>
                                         <strong><?php echo htmlentities($item['answer3']['tresc']);?></strong>
                                     <?php else : ?>
                                         <?php echo htmlentities($item['answer3']['tresc']);?>
                                     <?php endif; ?>
                                </span>
                            </div>
                        </div>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect4<?php echo $item['id']; ?>" id="isCorrect4<?php echo $item['id']; ?>" disabled
                                <?= $item['answer4']['marked']?'checked':'';?>/>
                            <label for="isCorrect4<?php echo $item['id']; ?>"></label>
                            <input type="hidden" name="answerId1" value="<?php echo $item['answers'][3]['id'];?>" />
                            <div class="col-10 px-0">
                                <span>
                                     <?php if($item['answer4']['czy_poprawne']) : ?>
                                         <strong><?php echo htmlentities($item['answer4']['tresc']);?></strong>
                                     <?php else : ?>
                                         <?php echo htmlentities($item['answer4']['tresc']);?>
                                     <?php endif; ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <?php endforeach; ?>
        </form>
</div>
<script src="/php.lc/assets/js/result-modal.js"></script>