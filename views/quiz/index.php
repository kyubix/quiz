<div class="quiz-header container">
    <h4>Egzamin z modułu</h4>
    <p>Przeczytaj uważnie pytanie, a następnie zaznacz odpowiedzi, które uważasz za poprawne. Nie zapomnij, że więcej niż jedna
        odpowiedź może być poprawna. Przycisk przekierowuje do dalszej części testu.</p>
    <hr />
    <br />
</div>
<div class="quiz-form col-md-10 offset-md-1">
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header bg-secondary text-light">
                    Pytanie nr <?= $_SESSION['i']+1; ?>
                </div>
                <div class="card-body">
                    <div class="form-group question-form">
                        <h5><?php echo htmlentities($viewmodel[$_SESSION['i']]['tresc']);?></h5>
                        <hr>
                    </div>
                    <?php if($viewmodel[$_SESSION['i']]['zdjecie']!= '') : ?>
                    <div class="col-lg-6 d-inline-block form-group question-form float-right">
                        <img class="img-thumbnail" src="data:image/jpeg;base64,<?= base64_encode($viewmodel[$_SESSION['i']]['zdjecie']);?>" alt="">
                    </div>
                    <?php endif; ?>
                    <?php if($viewmodel[$_SESSION['i']]['zdjecie']!= '') : ?>
                    <div class="col-lg-6 d-inline-block float-left">
                        <?php else : ?>
                        <div class="col-lg-12 d-inline-block">
                            <?php endif; ?>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect<?= $viewmodel[$_SESSION['i']]['answer1']['id']; ?>" id="isCorrect1<?= $viewmodel[$_SESSION['i']]['answer1']['id']; ?>"/>
                            <label for="isCorrect1<?= $viewmodel[$_SESSION['i']]['answer1']['id']; ?>"></label>
                            <div class="col-10 px-0">
                                <span><?php echo htmlentities($viewmodel[$_SESSION['i']]['answer1']['tresc']);?></span>
                            </div>
                        </div>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect<?= $viewmodel[$_SESSION['i']]['answer2']['id']; ?>" id="isCorrect2<?= $viewmodel[$_SESSION['i']]['answer2']['id']; ?>"/>
                            <label for="isCorrect2<?= $viewmodel[$_SESSION['i']]['answer2']['id']; ?>"></label>
                            <div class="col-10 px-0">
                                <span><?php echo htmlentities($viewmodel[$_SESSION['i']]['answer2']['tresc']);?></span>
                            </div>
                        </div>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect<?= $viewmodel[$_SESSION['i']]['answer3']['id']; ?>" id="isCorrect3<?= $viewmodel[$_SESSION['i']]['answer3']['id']; ?>"/>
                            <label for="isCorrect3<?= $viewmodel[$_SESSION['i']]['answer3']['id']; ?>"></label>
                            <div class="col-10 px-0">
                                <span><?php echo htmlentities($viewmodel[$_SESSION['i']]['answer3']['tresc']);?></span>
                            </div>
                        </div>
                        <div class="form-inline question-form">
                            <input type="checkbox" name="isCorrect<?= $viewmodel[$_SESSION['i']]['answer4']['id']; ?>" id="isCorrect4<?= $viewmodel[$_SESSION['i']]['answer4']['id']; ?>"/>
                            <label for="isCorrect4<?= $viewmodel[$_SESSION['i']]['answer4']['id']; ?>"></label>
                            <div class="col-10 px-0">
                                <span><?php echo htmlentities($viewmodel[$_SESSION['i']]['answer4']['tresc']);?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="text-center">
                <?php if($_SESSION['i']==29): ?>
                <input type="submit" id="submitForm" class="btn btn-primary font-weight-bold" name="submit" value="ZAKOŃCZ TEST"/>
                <?php else : ?>
                <input type="submit" id="submitForm" class="btn btn-primary" name="submit" value="Następne pytanie"/>
                <?php endif; ?>
            </div>
        </form>
</div>

<script>
    $('input:submit#submitForm').prop('disabled', true)
    $('form input:checkbox').change(function()
    {
        if( $('form input:checkbox:checked').length < 1)
        {
            $('input:submit#submitForm').fadeTo('fast', 0.5).prop('disabled', true);
        }
        else
        {
            $('input:submit#submitForm').fadeTo('fast', 1).prop('disabled', false);
        }
    });
</script>