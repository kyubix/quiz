<div class="home-page col-12">
    <div class="card home-card mb-5">
        <div class="text-white text-center py-5 px-4 my-5">
            <div class="jumbotron-image">
                <h1 class="card-title pt-3 mb-5 font-bold">Technologie sieci WEB</h1>
                <hr />
                <p style="font-size: 15px;" class="mx-5 my-5">Platforma testowa modułu prowadzonego na Wydziale Elektrotechniki i Informatyki Politechniki Rzeszowskiej
                    - wykładowca dr hab. inż. Jan PROKOP, prof. PRz.</p>
                <a class="btn btn-outline-primary" id="smooth-navigate"><i class="fa fa-clone left"></i> Zasady zaliczenia</a>
            </div>
        </div>
    </div>
    <!--Jumbotron-->
    <div class="jumbotron" id="rules">

        <h2 class="mb-4 blue-text">Technologie sieci WEB - zasady testu</h2>
        <p class="lead">Przed Tobą test zaliczeniowy z modułu Technologie sieci WEB. Poniżej znajduje się wszystkie potrzebne
            informacje oraz lista zasad obowiązująca podczas testu. Zapoznaj się z nią szczegółowo przed jego rozpoczęciem.</p>
        <hr class="my-4">
        <ul>
            <li>
                <p>Zestaw pytań zawiera 30 zadań.</p>
            </li>
            <li>
                <p>Każde zadanie składa się z pytania oraz z czterech możliwych odpowiedzi.</p>
            </li>
            <li>
                <p>Niektóre pytania mogą odnosić się do zdjęcia, które będzie dołączone do pytania.</p>
            </li>
            <li>
                <p>Jest to test <strong>wielokrotnego</strong> wyboru.</p>
            </li>
            <li>
                <p>Za każde poprawnie wypełnione pytanie można otrzymać jeden punkt.</p>
            </li>
            <li>
                <p>Oceny:</p>
                <p>- 30 - 29 pkt: <strong>5.0</strong></p>
                <p>- 28 - 26 pkt: <strong>4.5</strong></p>
                <p>- 25 - 23 pkt: <strong>4.0</strong></p>
                <p>- 22 - 20 pkt: <strong>3.5</strong></p>
                <p>- 19 - 18 pkt: <strong>3.0</strong></p>
                <p>- 17 - 0 pkt: <strong>&nbsp;2.0</strong></p>
            </li>
        </ul>
        <hr class="my-4">
        <p class="lead mb-4">Jeśli zapoznałeś się z wszystkimi informacjami, możesz rozpocząć wypełnianie testu poniższym przyciskiem.
            Spowoduje to wyświetlenie pierwszego pytania oraz wystartowanie czasu. Powodzenia!
        </p>
        <a class="btn btn-primary" href="<?php echo ROOT_URL; ?>/quiz/index">Rozpocznij wypełnianie testu</a>
    </div>
</div>

<script>
    $("#smooth-navigate").click(function() {
        $('html, body').animate({
            scrollTop: $("#rules").offset().top
        }, 1000);
    });
</script>
