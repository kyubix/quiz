<div class="question col-8 offset-2">
    <div class="question-heading">
        <h3>Nowe pytanie</h3>
        <p>Dodaj pytanie do bazy danych</p>
    </div>
    <div class="question-body">
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
            <div class="form-group question-form">
                <textarea name="question" class="form-control" placeholder="Treść pytania"></textarea>
                <div class="input-group image-preview">
                    <input type="text" class="form-control image-preview-filename mb-0 mr-1" disabled="disabled">
                    <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <i class="fa fa-remove clr-red"></i> Usuń
                    </button>
                        <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="fa fa-image"></span>
                        <span class="image-preview-input-title">Dodaj zdjęcie</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="questionImage"/>
                    </div>
                </span>
                </div>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect1" id="isCorrect1"/>
                <label for="isCorrect1"></label>
                <input type="text" name="answer1" class="form-control" placeholder="Opcja pierwsza"/>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect2" id="isCorrect2"/>
                <label for="isCorrect2"></label>
                <input type="text" name="answer2" class="form-control" placeholder="Opcja druga"/>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect3" id="isCorrect3"/>
                <label for="isCorrect3"></label>
                <input type="text" name="answer3" class="form-control" placeholder="Opcja trzecia"/>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect4" id="isCorrect4"/>
                <label for="isCorrect4"></label>
                <input type="text" name="answer4" class="form-control" placeholder="Opcja czwarta"/>
            </div>
            <input type="submit" class="btn btn-primary" name="submit" value="Zatwierdź"/>

        </form>
    </div>
</div>
<script src="/php.lc/assets/js/image-input.js"></script>