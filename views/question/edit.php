<div class="question col-8 offset-2">
    <div class="question-heading">
        <h3>Edytuj pytanie</h3>
        <p>Zmodyfikuj dowolne pola</p>
    </div>
    <div class="question-body">
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
            <div class="form-group question-form">
                <textarea name="question" class="form-control"  placeholder="Treść pytania"><?php echo htmlentities($viewmodel['tresc']);?></textarea>
                <?php if($viewmodel['zdjecie']=='') : ?>
                <div class="input-group image-preview">
                    <input type="text" class="form-control image-preview-filename mb-0 mr-1" disabled="disabled">
                    <span class="input-group-btn">
                        <!-- image-preview-clear button -->
                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                            <span class="fa fa-remove clr-red"></span> Usuń
                        </button>
                            <!-- image-preview-input -->
                        <div class="btn btn-default image-preview-input">
                            <span class="fa fa-image"></span>
                            <span class="image-preview-input-title">Dodaj zdjęcie</span>
                            <input type="file" accept="image/png, image/jpeg, image/gif" name="questionImage" />
                        </div>
                    </span>
                </div>
                <?php endif; ?>
            </div>
            <?php if($viewmodel['zdjecie']!='') : ?>
            <div class="question-form">
                <div id="image-div">
                    <img class="img-thumbnail d-block mx-auto" alt="" src="data:image/jpeg;base64,<?php echo base64_encode($viewmodel['zdjecie'] );?>"/>
                    <span id="deleteImageButton" class="btn btn-danger my-2"><i class="fa fa-trash"></i> Usuń zdjęcie</span>
                    <input type="checkbox" name="deleteImage" id="deleteImage"/>
                </div>
            </div>
            <?php endif; ?>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect1" <?php echo $viewmodel['answers'][0]['czy_poprawne']?'checked':'';?> id="isCorrect1"/>
                <label for="isCorrect1"></label>
                <input type="hidden" name="answerId1" value="<?php echo $viewmodel['answers'][0]['id'];?>" />
                <input type="text" name="answer1" value="<?php echo htmlentities($viewmodel['answers'][0]['tresc']);?>" class="form-control" placeholder="Opcja pierwsza"/>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect2" <?php echo $viewmodel['answers'][1]['czy_poprawne']?'checked':'';?> id="isCorrect2"/>
                <label for="isCorrect2"></label>
                <input type="hidden" name="answerId2" value="<?php echo $viewmodel['answers'][1]['id'];?>" />
                <input type="text" name="answer2" value="<?php echo htmlentities($viewmodel['answers'][1]['tresc']);?>" class="form-control" placeholder="Opcja druga"/>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect3" <?php echo $viewmodel['answers'][2]['czy_poprawne']?'checked':'';?> id="isCorrect3"/>
                <label for="isCorrect3"></label>
                <input type="hidden" name="answerId3" value="<?php echo $viewmodel['answers'][2]['id'];?>" />
                <input type="text" name="answer3" value="<?php echo htmlentities($viewmodel['answers'][2]['tresc']);?>" class="form-control" placeholder="Opcja trzecia"/>
            </div>
            <div class="form-inline question-form">
                <input type="checkbox" name="isCorrect4" <?php echo $viewmodel['answers'][3]['czy_poprawne']?'checked':'';?> id="isCorrect4"/>
                <label for="isCorrect4"></label>
                <input type="hidden" name="answerId4" value="<?php echo $viewmodel['answers'][3]['id'];?>" />
                <input type="text" name="answer4" value="<?php echo htmlentities($viewmodel['answers'][3]['tresc']);?>" class="form-control" placeholder="Opcja czwarta"/>
            </div>
            <input type="submit" class="btn btn-primary" name="submit" value="Zatwierdź"/>

        </form>
    </div>
</div>
<script src="/php.lc/assets/js/image-input.js"></script>
<script>
    $('#deleteImageButton').click(function()
    {
        $('#image-div').slideUp(1000);
        $('#deleteImage').prop('checked', true);
    })
</script>