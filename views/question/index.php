<div class="d-block col-12 col-lg-10 offset-lg-1 list-template">
    <a class="btn btn-primary" href="<?php echo ROOT_URL; ?>/admin/question/add"><i class="fa fa-plus mr-1"></i> Dodaj pytanie</a>
    <hr />
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th scope="col" width="35">ID</th>
                <th scope="col">Treść pytania</th>
                <th scope="col" width="65">Akcje</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4"></td>
            </tr>
            <?php foreach($viewmodel as $item) : ?>
            <tr>
                <td>
                    <div class="pt-1">
                    <a data-toggle="collapse" title="Rozwiń" href="#collapseItem<?php echo $item['id']; ?>" aria-expanded="false"
                       aria-controls="collapseItem<?php echo $item['id']; ?>">
                        <i class="fa fa-bars fa-lg clr-black"></i></a>
                    </div>
                </td>
                <td scope="row">
                    <?php echo $item['id']; ?>
                </td>
                <td>
                    <?php echo htmlentities($item['tresc']); ?>
                    <div class="float-right mt-1">
                    <?php if($item['zdjecie']!='') : ?>
                        <i class="fa fa-image fa-lg" title="Pytanie z obrazem"></i>
                    <?php endif; ?>
                    </div>
                </td>
                <td>
                    <a title="Edytuj" href="<?php echo ROOT_URL; ?>/admin/question/edit/<?php echo $item['id']; ?>"><i class="fa fa-edit fa-lg clr-primary"></i></a>
                    <a title="Usuń" href="#deleteQuestion<?php echo $item['id']; ?>" data-toggle="modal"><i class="fa fa-trash fa-lg clr-red"></i></a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <div class="collapse" id="collapseItem<?php echo $item['id']; ?>">
                        <div class="d-block">
                            <span class="fa fa-circle-o mr-4 mb-3 clr-primary"></span>
                            <span><?php echo htmlentities($item['answer1']['tresc']); ?></span>
                        </div>
                        <div class="d-block">
                            <span class="fa fa-circle-o mr-4 mb-3 clr-primary"></span>
                            <span><?php echo htmlentities($item['answer2']['tresc']); ?></span>
                        </div>
                        <div class="d-block">
                            <span class="fa fa-circle-o mr-4 mb-3 clr-primary"></span>
                            <span><?php echo htmlentities($item['answer3']['tresc']); ?></span>
                        </div>
                        <div class="d-block">
                            <span class="fa fa-circle-o mr-4 mb-3 clr-primary"></span>
                            <span><?php echo htmlentities($item['answer4']['tresc']); ?></span>
                        </div>
                    </div>
                </td>
            </tr>
                <!-- Modal -->
                <div class="modal fade" id="deleteQuestion<?php echo $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="deleteQuestionTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteQuestionTitle">Usuwanie pytania</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <span>Czy na pewno chcesz usunąć wybranie pytanie o ID: <?php echo $item['id']; ?>?</span>
                            </div>
                            <div class="modal-footer">
                                <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
                                    <input type="hidden" name="deleteItemId" value="<?php echo $item['id']; ?>">
                                    <input type="submit" class="btn btn-danger" name="submit" value="Usuń"/>
                                </form>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Wróć</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>