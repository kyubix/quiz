<html>
<head>
    <title>Quiz - platforma z testami</title>
    <link rel="stylesheet" href="/php.lc/assets/css/lib/bootstrap.css">
    <link rel="stylesheet" href="/php.lc/assets/css/lib/font-awesome.css">
    <link rel="stylesheet" href="/php.lc/assets/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="/php.lc/assets/js/global.js"></script>
</head>
<body>
<div id="main-navbar">
<nav class="navbar navbar-dark bg-dark fixed-top navbar-expand-sm py-2">
    <a class="navbar-brand py-0 bg-primary" href="/php.lc"><img src="/php.lc/img/logo-letter.png" width="35" height="35" class="d-inline-block align-middle" alt=""></a>
    <span class="mr-3 navbar-left clr-lightdark">Quiz - Technologie Sieci WEB</span>
    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#collapse-menu" aria-controls="collapse-menu" aria-expanded="false" aria-label="Przełącznik">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapse-menu">
        <div class="dropdown-divider"></div>
        <ul class="navbar-nav mr-auto">
            <?php if($_SESSION['user_data']['role']=='ADMIN') : ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo ROOT_URL; ?>/admin/question/index"> Pytania </a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="dropdown-divider"></div>
        <ul class="navbar-nav navbar-right">
            <?php if(isset($_SESSION['is_logged_in'])) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>"><?php echo $_SESSION['user_data']['login']; ?> <i class="fa fa-user-circle ml-1"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>/users/logout">Wyloguj <i class="fa fa-sign-out ml-1"></i></a>
                </li>
            <?php else : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>/users/login">Logowanie <i class="fa fa-sign-in fa ml-1"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>/users/register">Rejestracja <i class="fa fa-user-plus ml-1"></i></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
</div>

<div class="container main-content">
    <div class="row">
        <?php Messages::display(); ?>
        <?php require($view); ?>
    </div>
</div>

<footer class="main-footer container-fluid mt-5">
    <div class="row bg-dark py-4">
            <div class="col-md-5 offset-md-1">
                <p class="lead">INFORMACJE O STRONIE</p>
                <hr/>
                <p class="clr-lightdark">Strona stworzona na potrzeby modułu Technologie sieci WEB.</p>
                <p class="clr-lightdark">Autorzy: Kamil Ulaszek, Kamil Żak</p>
            </div>
            <div class="col-md-5">
                <p class="lead">LINKI</p>
                <hr/>
                <p><a class="clr-lightdark" href="http://web.prz.edu.pl/">Technologie sieci WEB - portal</a></p>
                <p><a class="clr-lightdark" href="https://w.prz.edu.pl/">Strona główna Politechniki Rzeszowskiej</a></p>
                <p><a class="clr-lightdark" href="https://weii.prz.edu.pl/">Politechnika Rzeszowska - WEiI</a></p>
            </div>
    </div>
    <div class="row bg-strongdark py-4">
        <div class="col-12 text-center">
            <span class="text-secondary">© Copyright:</span>
            <span class="clr-lightdark"> Politechnika Rzeszowska im. Ignacego Łukasiewicza 2019</span>
        </div>
    </div>
</footer>

<script src="/php.lc/assets/js/lib/popper.js"></script>
<script src="/php.lc/assets/js/lib/bootstrap.js"></script>
</body>
</html>