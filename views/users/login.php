<div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-8 offset-2">
    <div class="card auth login">
        <div class="card-header">
            <i class="fa fa-user-circle fa-4x"></i><br />
            <h3> Logowanie </h3>
        </div>
        <div class="card-body">
            <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    <input type="text" name="login" class="form-control" placeholder="Login"/>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control mb-4" placeholder="Hasło"/>
                </div>
                <input type="submit" class="btn btn-primary" name="submit" value="Zaloguj"/>

            </form>
        </div>
    </div>
</div>