<div class="col-md-6 offset-md-3 col-8 offset-2">
    <div class="card auth register">
        <div class="card-header">
            <i class="fa fa-user-plus fa-4x"></i><br />
            <h3> Rejestracja </h3>
        </div>
        <div class="card-body">
            <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    <input type="text" name="login" class="form-control" placeholder="Login"/>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Hasło"/>
                </div>
                <div class="form-group">
                    <input type="text" name="email" class="form-control mb-4" placeholder="Adres e-mail"/>
                </div>
                <input type="submit" class="btn btn-primary auth" name="submit" value="Zarejestruj"/>

            </form>
        </div>
    </div>
</div>
