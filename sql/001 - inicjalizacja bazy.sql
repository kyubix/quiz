CREATE TABLE `quiz`.`uzytkownik` ( `id` INT NOT NULL AUTO_INCREMENT , `login` VARCHAR(255) NOT NULL , `email` VARCHAR(255) NOT NULL , `haslo` VARCHAR(255) NOT NULL , `id_rola` INT NOT NULL , `id_statystyka` INT NOT NULL , `data_zalozenia` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `quiz`.`rola` ( `id` INT NOT NULL AUTO_INCREMENT , `nazwa` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `quiz`.`statystyka` ( `id` INT NOT NULL AUTO_INCREMENT , `liczba_quizow` INT NOT NULL , `poprawne_odpowiedzi` INT NOT NULL , `id_uzytkownik` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `quiz`.`pytanie` ( `id` INT NOT NULL AUTO_INCREMENT , `tresc` VARCHAR(1023) NOT NULL , `data_dodania` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `id_uzytkownik` INT NOT NULL , `zdjecie` MEDIUMBLOB NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `quiz`.`odpowiedz` ( `id` INT NOT NULL AUTO_INCREMENT , `tresc` VARCHAR(16383) NOT NULL , `czy_poprawne` BOOLEAN NOT NULL , `id_pytanie` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `quiz`.`rola` (`id`, `nazwa`) VALUES (NULL, 'ADMIN'), (NULL, 'USER')
