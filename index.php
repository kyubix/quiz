<?php
//start session
session_start();
//Include Config
require('config.php');
require('classes/Bootstrap.php');
require('classes/Controller.php');
require ('classes/Model.php');
require ('classes/Messages.php');

require('models/home.php');
require('models/user.php');
require('models/question.php');

require('controllers/home.php');
require('controllers/users.php');
require('controllers/admin/question.php');
require('controllers/quiz.php');

$bootstrap = new Bootstrap($_GET);
$controller = $bootstrap->createController();
if($controller){
    $controller->executeAction();
}